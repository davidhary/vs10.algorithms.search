﻿''' <summary> Defines an interface for a search algorithm. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Interface ISearch(Of TArgument As {Structure, IFormattable, IComparable(Of TArgument)},
                             TValue As {Structure, IFormattable, IComparable(Of TValue)})

    ''' <summary>
    ''' Performs the search.
    ''' </summary>
    ''' <param name="left">The left argument.</param>
    ''' <param name="right">The right argument.</param>
    ''' <param name="argumentPrecision">The absolute difference between the arguments.</param>
    ''' <param name="valuePrecision">The value precision.</param>
    ''' <param name="slope">One if function is increasing, -1 if decreasing, 0 if either increasing or decreasing.</param>
    Function Search(ByVal left As TArgument, ByVal right As TArgument, ByVal argumentPrecision As TArgument,
                    ByVal valuePrecision As TValue, ByVal slope As Integer) As TArgument

    ''' <summary>
    ''' Perform the search starting with the specified search arguments.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Function Search(ByVal args As SearchArgs(Of TArgument, TValue)) As SearchArgs(Of TArgument, TValue)

    ''' <summary>
    ''' Perform a single step search.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Function StepSearch(ByVal args As SearchArgs(Of TArgument, TValue)) As SearchArgs(Of TArgument, TValue)

    ''' <summary>
    ''' Gets or sets reference to the entity that implements the 
    ''' <see cref="IProcessFunction">process function interface</see>.
    ''' </summary>
    Property ProcessFunction As IProcessFunction(Of TArgument, TValue)

End Interface

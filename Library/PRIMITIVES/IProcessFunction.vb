﻿''' <summary>
''' Defines the process function which is subject to the search.
''' </summary>
''' <remarks>
''' This entity receives a single value. It then returns the corresponding function value.
''' For example, this could be the difference between a desired value and an actual value.
''' The search attempts to attain a minimum of difference between the two. 
''' </remarks>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Interface IProcessFunction(Of TArgument As {Structure, IFormattable, IComparable(Of TArgument)},
                                      TValue As {Structure, IFormattable, IComparable(Of TValue)})

    ''' <summary>
    ''' Evaluates the specified value.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>The value or nothing if the value failed to evaluate.</returns>
    Function Evaluate(ByVal value As ArgumentValuePair(Of TArgument, TValue)) As ArgumentValuePair(Of TArgument, TValue)

End Interface

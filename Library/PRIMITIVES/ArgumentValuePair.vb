﻿Imports System.ComponentModel

''' <summary> The container for the search arguments. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <typeparam name="TArgument"> The type of the argument. </typeparam>
''' <typeparam name="TValue">    The type of the value. </typeparam>
Public Class ArgumentValuePair(Of TArgument As {Structure, IFormattable, IComparable(Of TArgument)},
                                  TValue As {Structure, IFormattable, IComparable(Of TValue)})

    ''' <summary> Initializes a new instance of the <see cref="ArgumentValuePair" /> class. </summary>
    ''' <param name="argument"> The argument. </param>
    Public Sub New(ByVal argument As TArgument)
        Me.New(argument, New TValue?)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ArgumentValuePair" /> class.
    ''' </summary>
    ''' <param name="argument">The argument.</param>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal argument As TArgument, ByVal value As TValue?)
        MyBase.New()
        Me.Argument = argument
        Me.ArgumentValue = value
    End Sub

    ''' <summary>
    ''' Gets or sets the argument.
    ''' </summary>
    ''' <value>
    ''' The argument.
    ''' </value>
    Public Property Argument As TArgument

    ''' <summary>
    ''' Gets or sets the argument value.
    ''' </summary>
    ''' <value>
    ''' The argument value.
    ''' </value>
    Public Property ArgumentValue As Nullable(Of TValue)

    ''' <summary>
    ''' Gets or sets the state of the search.
    ''' </summary>
    ''' <value>
    ''' The state of the search.
    ''' </value>
    Public Property SearchState As SearchState

    ''' <summary>
    ''' Determines whether the search completed either successfully or not.
    ''' </summary>
    ''' <returns>
    '''   <c>True</c> if has value or error; otherwise, <c>False</c>.
    ''' </returns>
    Public Function HasCompleted() As Boolean
        Return Me.ArgumentValue.HasValue OrElse SearchState <> Search.SearchState.None
    End Function

End Class

''' <summary>
''' The container for integer search arguments.
''' </summary>
Public Class IntegerArgumentValuePair
    Inherits ArgumentValuePair(Of Integer, Integer)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="IntegerArgumentValuePair" /> class.
    ''' </summary>
    ''' <param name="argument">The argument.</param>
    Public Sub New(ByVal argument As Integer)
        MyBase.New(argument)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="IntegerArgumentValuePair" /> class.
    ''' </summary>
    ''' <param name="argument">The argument.</param>
    ''' <param name="value">The value.</param>
    Public Sub New(ByVal argument As Integer, ByVal value As Integer)
        MyBase.New(argument, value)
    End Sub

End Class

''' <summary>
''' Enumerates the search state.
''' </summary>
Public Enum SearchState
    <Description("Not Completed")> None
    ''' <summary>
    ''' Search aborted.
    ''' </summary>
    <Description("Aborted")> Aborted
    ''' <summary>
    ''' Search encountered an unrecoverable error.
    ''' </summary>
    <Description("Had Error")> HadError
    ''' <summary>
    ''' Search hit the limit on the number of iterations or function evaluations.
    ''' </summary>
    <Description("Hit `Iteration Count Limit")> HitIterationCountLimit
    ''' <summary>
    ''' Argument within the argument precision range.
    ''' </summary>
    <Description("Approximates Argument")> ApproximatesArgument
    ''' <summary>
    ''' Value within the argument value precision range.
    ''' </summary>
    <Description("Approximated Value")> ApproximatesValue
End Enum
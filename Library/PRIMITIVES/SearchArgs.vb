﻿
''' <summary>
''' The container for the search arguments.
''' </summary>
''' <typeparam name="TArgument">The type of the argument.</typeparam>
''' <typeparam name="TValue">The type of the value.</typeparam>
Public Class SearchArgs(Of TArgument As {Structure, IFormattable, IComparable(Of TArgument)},
                            TValue As {Structure, IFormattable, IComparable(Of TValue)})

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SearchArgs(Of TArgument,TValue)" /> class.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param>
    ''' <param name="argumentPrecision">The precision of the argument.</param>
    ''' <param name="valuePrecision">The precision of the value.</param>
    ''' <param name="slope">The slope.</param>
    Public Sub New(ByVal left As ArgumentValuePair(Of TArgument, TValue),
                   ByVal right As ArgumentValuePair(Of TArgument, TValue),
                   ByVal argumentPrecision As TArgument, ByVal valuePrecision As TValue, ByVal slope As Integer)
        MyBase.New()
        Me._Left = left
        Me._Right = right
        Me._Root = New ArgumentValuePair(Of TArgument, TValue)(left.Argument)
        Me._ArgumentPrecision = argumentPrecision
        Me._ValuePrecision = valuePrecision
        Me._Slope = slope
    End Sub

    ''' <summary>
    ''' Gets or sets the left argument value pair.
    ''' </summary>
    ''' <value>
    ''' The left.
    ''' </value>
    Public Property Left As ArgumentValuePair(Of TArgument, TValue)

    ''' <summary>
    ''' Gets or sets the right.
    ''' </summary>
    ''' <value>
    ''' The right.
    ''' </value>
    Public Property Right As ArgumentValuePair(Of TArgument, TValue)

    ''' <summary>
    ''' Gets or sets the argument precision.
    ''' </summary>
    ''' <value>
    ''' The precision.
    ''' </value>
    Public Property ArgumentPrecision As TArgument

    ''' <summary>
    ''' Gets or sets the value precision.
    ''' </summary>
    ''' <value>
    ''' The precision.
    ''' </value>
    Public Property ValuePrecision As TValue

    ''' <summary>
    ''' Gets or sets the slope.
    ''' </summary>
    ''' <value>
    ''' The slope.
    ''' </value>
    Public Property Slope As Integer

    ''' <summary>
    ''' Gets or sets the search outcome.
    ''' </summary>
    Public Property Root As ArgumentValuePair(Of TArgument, TValue)

End Class

''' <summary>
''' The Integer search arguments.
''' </summary>
Public Class SearchIntegerArgs
    Inherits SearchArgs(Of Integer, Integer)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SearchArgs(Of integer,integer)" /> class.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param>
    ''' <param name="argumentPrecision">The argument precision.</param>
    ''' <param name="valuePrecision">The value precision</param>
    ''' <param name="slope">The slope.</param>
    Public Sub New(ByVal left As IntegerArgumentValuePair, ByVal right As IntegerArgumentValuePair,
                   ByVal argumentPrecision As Integer, ByVal valuePrecision As Integer,
                   ByVal slope As Integer)
        MyBase.New(left, right, argumentPrecision, valuePrecision, slope)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SearchIntegerArgs" /> class.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param>
    ''' <param name="argumentPrecision">The argument precision.</param>
    ''' <param name="valuePrecision">The value precision.</param>
    ''' <param name="slope">The slope.</param>
    Public Sub New(ByVal left As Integer, ByVal right As Integer,
                   ByVal argumentPrecision As Integer, ByVal valuePrecision As Integer,
                   ByVal slope As Integer)
        MyBase.New(New IntegerArgumentValuePair(left), New IntegerArgumentValuePair(right), argumentPrecision, valuePrecision, slope)
    End Sub

End Class
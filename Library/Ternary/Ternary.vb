﻿''' <summary> Performs a ternary search. </summary>
''' <remarks> The ternary search implemented here is capable of initializing based on the
''' difference between the two initial values. For more information see Wikipedia Ternary Search:
''' http://en.wikipedia.org/wiki/Ternary_search. </remarks>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public MustInherit Class Ternary(Of TArgument As {Structure, IFormattable, IComparable(Of TArgument)},
                         TValue As {Structure, IFormattable, IComparable(Of TValue)})
    Implements ISearch(Of TArgument, TValue)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Ternary(Of TArgument, TValue)" /> class.
    ''' </summary>
    ''' <param name="processFunction">The process function.</param>
    Protected Sub New(ByVal processFunction As IProcessFunction(Of TArgument, TValue))
        MyBase.New()
        Me._ProcessFunction = processFunction
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the entity that implements the
    ''' <see cref="IProcessFunction">process function interface</see>.
    ''' </summary>
    ''' <value>The process function.</value>
    Protected Property ProcessFunction As IProcessFunction(Of TArgument, TValue) Implements ISearch(Of TArgument, TValue).ProcessFunction

    ''' <summary>
    ''' Performs the search.
    ''' </summary>
    ''' <param name="left">The left argument.</param>
    ''' <param name="right">The right argument.</param>
    ''' <param name="argumentPrecision">The absolute difference between the arguments.</param>
    ''' <param name="slope">One if function is increasing, -1 if decreasing, 0 if either increasing or decreasing.</param>
    Public MustOverride Function Search(left As TArgument, right As TArgument, argumentPrecision As TArgument,
                                        ByVal valuePrecision As TValue, ByVal slope As Integer) As TArgument Implements ISearch(Of TArgument, TValue).Search

    ''' <summary>
    ''' Perform the search starting with the specified search arguments.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Public MustOverride Function Search(args As SearchArgs(Of TArgument, TValue)) As SearchArgs(Of TArgument, TValue) Implements ISearch(Of TArgument, TValue).Search

    ''' <summary>
    ''' Perform a single step search.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Public MustOverride Function StepSearch(ByVal args As SearchArgs(Of TArgument, TValue)) As SearchArgs(Of TArgument, TValue) Implements ISearch(Of TArgument, TValue).StepSearch

End Class


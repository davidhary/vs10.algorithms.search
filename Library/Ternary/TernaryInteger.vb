﻿Imports System.Runtime.CompilerServices

''' <summary> Perform a ternary search on integers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class TernaryInteger
    Inherits Ternary(Of Integer, Integer)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Ternary(Of TArgument, TValue)" /> class.
    ''' </summary>
    ''' <param name="processFunction">The process function.</param>
    Public Sub New(ByVal processFunction As IProcessFunction(Of Integer, Integer))
        MyBase.New(processFunction)
    End Sub

    ''' <summary>
    ''' Performs the search.
    ''' </summary>
    ''' <param name="left">The left argument.</param>
    ''' <param name="right">The right argument.</param>
    ''' <param name="argumentPrecision">The absolute difference between the arguments.</param>
    ''' <param name="valuePrecision">The value precision.</param>
    ''' <param name="slope">One if function is increasing, -1 if decreasing, 0 if either increasing or decreasing.</param>
    ''' <returns>
    ''' The sought after function value
    ''' </returns>
    Public Overrides Function Search(left As Integer, right As Integer, argumentPrecision As Integer, valuePrecision As Integer, ByVal slope As Integer) As Integer
        Dim e As New SearchArgs(Of Integer, Integer)(New IntegerArgumentValuePair(left), New IntegerArgumentValuePair(right), argumentPrecision, valuePrecision, slope)
        Return Me.Search(e).Root.Argument
    End Function

    ''' <summary>
    ''' Perform the search starting with the specified search arguments.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Public Overloads Overrides Function Search(ByVal args As SearchArgs(Of Integer, Integer)) As SearchArgs(Of Integer, Integer)
        Do Until args.Root.HasCompleted
            args = StepSearch(args)
        Loop
        Return args
    End Function

    ''' <summary>
    ''' Perform a single step search.
    ''' </summary>
    ''' <param name="args">The search arguments.</param>
    Public Overloads Overrides Function StepSearch(ByVal args As SearchArgs(Of Integer, Integer)) As SearchArgs(Of Integer, Integer)

        Dim stepSize As Integer = args.Right.Argument - args.Left.Argument
        Do Until args.Left.HasCompleted
            args.Left = Me.ProcessFunction.Evaluate(args.Left)
        Loop
        If Not args.Left.ArgumentValue.HasValue Then
            ' if no value, either completed or had error 
            args.Root = args.Left
            Return args
        End If
        Do Until args.Right.HasCompleted
            args.Right = Me.ProcessFunction.Evaluate(args.Right)
        Loop
        If Not args.Right.ArgumentValue.HasValue Then
            ' if no value, either completed or had error 
            args.Root = args.Right
            Return args
        End If
        If args.Left.ArgumentValue.Value.Approximates(args.ValuePrecision) Then
            ' left has it -- calculate the final value.
            args.Root = MyBase.ProcessFunction.Evaluate(args.Left)
            args.Root.SearchState = SearchState.ApproximatesValue
        ElseIf args.Right.ArgumentValue.Value.Approximates(args.ValuePrecision) Then
            ' right has it; calculate the final value.
            args.Root = MyBase.ProcessFunction.Evaluate(args.Right)
            args.Root.SearchState = SearchState.ApproximatesValue
        ElseIf (args.Right.Argument - args.Left.Argument) <= args.ArgumentPrecision Then
            ' left and right are close
            If args.Left.ArgumentValue = args.Right.ArgumentValue Then
                ' if the left and right are the same (unlikely() return the middle
                args.Root.Argument = CInt(0.5 * (args.Right.Argument + args.Left.Argument))
            Else
                ' otherwise, interpolate.
                args.Root.Argument = args.Left.Argument - CInt(args.Left.ArgumentValue * (args.Right.Argument - args.Left.Argument) /
                                                                                         (args.Right.ArgumentValue - args.Left.ArgumentValue))
            End If
            ' must calculate the final value.
            args.Root = MyBase.ProcessFunction.Evaluate(args.Root)
            args.Root.SearchState = SearchState.ApproximatesArgument
        ElseIf args.Left.ArgumentValue.Value.CompareTo(0) = args.Right.ArgumentValue.Value.CompareTo(0) Then
            ' left and right are the current bounds; the location of the root is outside the range
            If args.Left.ArgumentValue.Value.CompareTo(0) > 0 Then
                ' value is above the function
                Select Case args.Slope
                    Case 1
                        ' with positive slope, move left
                        args.Right.ArgumentValue = args.Left.ArgumentValue.Value
                        args.Left.ArgumentValue = New Integer?
                        args.Right.Argument = args.Left.Argument
                        args.Left.Argument = args.Left.Argument - stepSize
                    Case 0
                        ' with unknown slope, move based on the closer value.
                        Dim leftCompareRight As Integer = args.Left.ArgumentValue.Value.CompareTo(args.Right.ArgumentValue.Value)
                        Select Case leftCompareRight
                            Case 1
                                ' if left is larger, move right 
                                args.Left.ArgumentValue = args.Right.ArgumentValue.Value
                                args.Right.ArgumentValue = New Integer?
                                args.Left.Argument = args.Right.Argument
                                args.Right.Argument = args.Right.Argument + stepSize
                            Case 0
                                ' if left and right have the same value, expand the range.
                                args.Left.Argument = args.Left.Argument - stepSize
                                args.Right.Argument = args.Right.Argument + stepSize
                                args.Left.ArgumentValue = New Integer?
                                args.Right.ArgumentValue = New Integer?
                            Case -1
                                ' if right is larger, move left
                                args.Right.ArgumentValue = args.Left.ArgumentValue.Value
                                args.Left.ArgumentValue = New Integer?
                                args.Right.Argument = args.Left.Argument
                                args.Left.Argument = args.Left.Argument - stepSize
                        End Select
                    Case -1
                        ' with negative slope, move right
                        args.Left.ArgumentValue = args.Right.ArgumentValue.Value
                        args.Right.ArgumentValue = New Integer?
                        args.Left.Argument = args.Right.Argument
                        args.Right.Argument = args.Right.Argument + stepSize
                End Select
            Else
                ' value is below the function
                Select Case args.Slope
                    Case 1
                        ' if slope is positive, move right
                        args.Left.ArgumentValue = args.Right.ArgumentValue.Value
                        args.Right.ArgumentValue = New Integer?
                        args.Left.Argument = args.Right.Argument
                        args.Right.Argument = args.Right.Argument + stepSize
                    Case 0
                        ' is slope is unknown, use the closer value
                        Dim leftCompareRight As Integer = args.Left.ArgumentValue.Value.CompareTo(args.Right.ArgumentValue.Value)
                        Select Case leftCompareRight
                            Case 1
                                ' left is larger, move left
                                args.Right.ArgumentValue = args.Left.ArgumentValue.Value
                                args.Left.ArgumentValue = New Integer?
                                args.Right.Argument = args.Left.Argument
                                args.Left.Argument = args.Left.Argument - stepSize
                            Case 0
                                ' if left and right have the same value, expand the range.
                                args.Left.Argument = args.Left.Argument - stepSize
                                args.Right.Argument = args.Right.Argument + stepSize
                                args.Left.ArgumentValue = New Integer?
                                args.Right.ArgumentValue = New Integer?
                            Case -1
                                ' right is larger, move right
                                args.Left.ArgumentValue = args.Right.ArgumentValue.Value
                                args.Right.ArgumentValue = New Integer?
                                args.Left.Argument = args.Right.Argument
                                args.Right.Argument = args.Right.Argument + stepSize
                        End Select
                    Case -1
                        ' if slope is negative, move left
                        args.Right.ArgumentValue = args.Left.ArgumentValue.Value
                        args.Left.ArgumentValue = New Integer?
                        args.Right.Argument = args.Left.Argument
                        args.Left.Argument = args.Left.Argument - stepSize
                End Select

            End If
        Else
            ' left and right are the current bounds; the solution is between them
            Dim leftThird As New ArgumentValuePair(Of Integer, Integer)(args.Left.Argument + CInt(stepSize / 3))
            Do
                leftThird = MyBase.ProcessFunction.Evaluate(leftThird)
            Loop Until leftThird.HasCompleted
            If Not leftThird.ArgumentValue.HasValue Then
                ' if no value, either completed or had error 
                args.Root = leftThird
                Return args
            End If

            If leftThird.ArgumentValue.Value.Approximates(args.ValuePrecision) Then
                ' left has it; this will terminate on the next iteration.
                args.Right = leftThird
            ElseIf args.Left.ArgumentValue.Value.CompareTo(0) <> leftThird.ArgumentValue.Value.CompareTo(0) Then
                ' value between left and left third
                ' left has it
                args.Right = leftThird
            Else

                Dim rightThird As New ArgumentValuePair(Of Integer, Integer)(args.Right.Argument - CInt(stepSize / 3))
                Do
                    rightThird = Me.ProcessFunction.Evaluate(rightThird)
                Loop Until rightThird.HasCompleted
                If Not rightThird.ArgumentValue.HasValue Then
                    ' if no value, either completed or had error 
                    args.Root = rightThird
                    Return args
                End If
                If rightThird.ArgumentValue.Value.Approximates(args.ValuePrecision) Then
                    ' right has it; this will terminate on the next iteration.
                    args.Left = rightThird
                ElseIf leftThird.ArgumentValue.Value.CompareTo(0) <> rightThird.ArgumentValue.Value.CompareTo(0) Then
                    ' value is between left and right thirds.
                    args.Right = rightThird
                    args.Left = leftThird
                ElseIf rightThird.ArgumentValue.Value.CompareTo(0) <> args.Right.ArgumentValue.Value.CompareTo(0) Then
                    ' value is between right and right third.
                    args.Left = rightThird
                Else
                    Debug.Assert(Not Debugger.IsAttached, "Unhandled case", "Unhandled case 4 tuples ({0},{1}) ({2},{3}) ({4},{5}) ({6},{7})",
                                 args.Left.Argument, args.Left.ArgumentValue,
                                 leftThird.Argument, leftThird.ArgumentValue,
                                 rightThird.Argument, rightThird.ArgumentValue,
                                 args.Right.Argument, args.Right.ArgumentValue)
                End If
            End If
        End If
        Return args

    End Function

End Class

Public Module Extensions

    ''' <summary>
    ''' Returns True if the value approximates the reference within some delta.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="reference">The reference.</param>
    ''' <param name="delta">The delta.</param>
    <Extension()>
    Public Function Approximates(ByVal value As Integer, ByVal reference As Integer, ByVal delta As Integer) As Boolean
        Return Math.Abs(value - reference) <= delta
    End Function

    ''' <summary>
    ''' Returns True if the value approximates zero within some delta.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="delta">The delta.</param>
    <Extension()>
    Public Function Approximates(ByVal value As Integer, ByVal delta As Integer) As Boolean
        Return value.Approximates(0, delta)
    End Function

End Module
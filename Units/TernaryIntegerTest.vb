﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Algorithms.Search



'''<summary>
'''This is a test class for TernaryIntegerTest and is intended
'''to contain all TernaryIntegerTest Unit Tests
'''</summary>
<TestClass()> 
Public Class TernaryIntegerTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for Search
    '''</summary>
    <TestMethod()> 
    Public Sub SearchTest()
        Dim left As Integer = 20
        Dim right As Integer = 40
        Dim absolutePrecision As Integer = 1
        Dim expected As Integer = 431
        Dim actual As Integer
        Dim searchFunction As BiasCurrent = New BiasCurrent
        Dim target As TernaryInteger = New TernaryInteger(searchFunction)
        actual = target.Search(left, right, absolutePrecision, 0, 1)
        Assert.AreEqual(expected, actual)
        Assert.AreEqual(0, searchFunction.FunctionEvaluations)
    End Sub

End Class

Public Class BiasCurrent
    Implements IProcessFunction(Of Integer, Integer)


    Private _FunctionEvaluations As Integer
    Public ReadOnly Property FunctionEvaluations As Integer
        Get
            Return Me._FunctionEvaluations
        End Get
    End Property

    Public Function Evaluate(value As isr.Algorithms.Search.ArgumentValuePair(Of Integer, Integer)) As isr.Algorithms.Search.ArgumentValuePair(Of Integer, Integer) Implements isr.Algorithms.Search.IProcessFunction(Of Integer, Integer).Evaluate
        Me._FunctionEvaluations += 1
        Dim minAdc As Integer = 0
        Dim maxAdc As Integer = 4091
        Dim minCurrent As Integer = 0
        Dim maxCurrent As Integer = 1000
        Dim initialCurrent As Integer = 145
        Dim targetOffsetCurrent As Integer = 250
        Dim newCurrent As Integer = (value.Argument - minAdc) * (maxCurrent - minCurrent) / (maxAdc - minAdc)
        Return New IntegerArgumentValuePair(newCurrent - (targetOffsetCurrent - initialCurrent), 1)
    End Function
End Class
